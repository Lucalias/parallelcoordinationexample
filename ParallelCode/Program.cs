﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ParallelCode
{
    class Program
    {
        //Why Synchronise?

        static void Main(string[] args)
        {
            Console.WriteLine("Magic Add 1:");
            MagicAddition magicAdd1 = new MagicAddition();
            magicAdd1.NoConcurrency(1000000);
            magicAdd1.PrintMagicInts();

            Console.WriteLine();

            Console.WriteLine("Magic Add 2:");
            MagicAddition magicAdd2 = new MagicAddition();
            magicAdd2.StupidConcurrency(1000000);
            magicAdd2.PrintMagicInts();

            Console.WriteLine();

            Console.WriteLine("Magic Add 3:");
            MagicAddition magicAdd3 = new MagicAddition();
            magicAdd3.StupidParallelFor(1000000);
            magicAdd3.PrintMagicInts();

            Console.WriteLine();

            Console.WriteLine("Magic Add 4:");
            MagicAddition magicAdd4 = new MagicAddition();
            magicAdd4.NastyLocks(1000000);
            magicAdd4.PrintMagicInts();

            Console.WriteLine();

            Console.WriteLine("Magic Add 5:");
            MagicAddition magicAdd5 = new MagicAddition();
            magicAdd5.LockFreeAtomicOperations(1000000);
            magicAdd5.PrintMagicInts();

            Console.WriteLine();

            Console.WriteLine("Magic Add 6:");
            MagicAddition magicAdd6 = new MagicAddition();
            magicAdd6.LockFreeLocalStorage(1000000);
            magicAdd6.PrintMagicInts();

            Console.ReadKey();

        }


    }
}
